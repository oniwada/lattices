#ifndef __ND_SquareArray__H
#define __ND_SquareArray__H

#include<cmath>
#include<iterator>
#include<assert.h>
#include"_MPI_vector_.h"

namespace _ND_SquareArray_func_{
// template <typename Type>
// class _ND_SquareArray_Indexes_{

// }
}

template <typename Type>
class _ND_SquareArray_{
	public:
 typedef uint_fast64_t _Def_size_;
/*	private:*/
 _Def_size_ Dim, size;
 Type *Arr = NULL;

/*	public:*/
 _ND_SquareArray_();
 _ND_SquareArray_(_Def_size_, _Def_size_);
 ~_ND_SquareArray_(void);

 void Realloc(_Def_size_, _Def_size_);
 void Free(void);
 inline void Set_to(uint8_t);
 inline uint_fast64_t get_size(void) const;
 inline Type  operator[](_Def_size_*) const;
 inline Type& operator[](_Def_size_*);
 inline Type  operator[](uint16_t*) const;
 inline Type& operator[](uint16_t*);
// inline Type  operator[](_Def_size_&) const;
// inline Type& operator[](_Def_size_&);
};

template<typename Type>
_ND_SquareArray_<Type>::_ND_SquareArray_(){
	Arr = (Type*)calloc(pow(1,1),sizeof(Type));
	this->Dim = 1;
	this->size = 1;
}

template<typename Type>
_ND_SquareArray_<Type>::_ND_SquareArray_(_Def_size_ Dim, _Def_size_ size){
	Arr = (Type*)calloc(pow(size,Dim),sizeof(Type));
	this->Dim = Dim;
	this->size = size;
}

template<typename Type>
_ND_SquareArray_<Type>::~_ND_SquareArray_(){
 Free();
}

template<typename Type>
void _ND_SquareArray_<Type>::Realloc(_Def_size_ new_Dim, _Def_size_ new_size){
 assert(new_size > 0U && new_Dim > 0 );
 Arr = (Type*) realloc(Arr, sizeof(Type)*pow(new_size,new_Dim));
 if(Arr == NULL){
	fprintf(stderr,"error reallocating Arr in ND square lattice\n");
	exit(1);
 }
 this->Dim = new_Dim;
 this->size = new_size;
}

template<typename Type>
void _ND_SquareArray_<Type>::Free(void){
 free(Arr);
 Arr = NULL;
 this->size = 0;
 this->Dim = 0;
}

template<typename Type>
inline Type& _ND_SquareArray_<Type>::operator[](_Def_size_ *start){
 _Def_size_ Index = *start;
 _Def_size_ u = (_Def_size_) 1U;
 for(; u < Dim; u++){
	Index += pow(size, u)*(*(start+u));
 }
 return Arr[Index];
}

template<typename Type>
inline Type _ND_SquareArray_<Type>::operator[](_Def_size_ *start) const{
 _Def_size_ Index = *start;
 _Def_size_ u = 1U;
 for(; u < Dim; u++){
	Index += pow(size, u)*(*(start+u));
 }
 return Arr[Index];
}

template<typename Type>
inline Type& _ND_SquareArray_<Type>::operator[](uint16_t *start){
 uint64_t Index = *start;
 uint64_t u = (_Def_size_) 1U;
 for(; u < Dim; u++){
	Index += pow(size, u)*((uint64_t) *(start+u));
 }
 return Arr[Index];
}

template<typename Type>
inline Type _ND_SquareArray_<Type>::operator[](uint16_t *start) const{
 uint64_t Index = *start;
 uint64_t u = 1U;
 for(; u < Dim; u++){
	Index += pow(size, u)*((uint64_t) *(start+u));
 }
 return Arr[Index];
}

template<typename Type>
inline uint_fast64_t _ND_SquareArray_<Type>::get_size(void) const{
 return size;
}

template<typename Type>
inline void _ND_SquareArray_<Type>::Set_to(uint8_t value){
 memset(Arr, value, sizeof(Type)*pow(size, Dim));
}
#endif

