#ifndef _2bitsNDArray__H
#define _2bitsNDArray__H

#include<vector>
#include<algorithm>
#include<math.h>
#include<assert.h>


class _2bitsNDArray_{
	private:
// typedef uint64_t ArrType;
 const uint8_t MaskTo0[4]  = {0xFC, 0xF3, 0xCF, 0x3F};
 const uint8_t Mask0To1[4] = {0x01, 0x04, 0x10, 0x40};
 const uint8_t Mask3To1[4] = {0xFD, 0xF7, 0xDF, 0x7F};
 const uint8_t Mask3To2[4] = {0xFE, 0xFB, 0xEF, 0xBF};
 const uint8_t MaskGet[4] = {0x03, 0x0C, 0x30, 0xC0};
 typedef uint8_t ArrType;
 uint64_t Dim = 0,
          Size = 0,
          LineSize = 0,
          AllocatedSize = 0;
 ArrType *Array = NULL;
// const uint64_t ElePerItem = 31;
// const uint64_t shifter = 5;
 const uint64_t ElePerItem = 3;
 const uint64_t shifter = 2;
// const uint64_t Nbits = sizeof(ArrType) << 3;
// const ArrType bits = 2U;

	public:
 _2bitsNDArray_(void);
 _2bitsNDArray_(uint64_t, uint64_t);
 ~_2bitsNDArray_();

 inline uint64_t size(void) const;
 void Realloc(uint64_t, uint64_t);
 void Free(void);
 inline uint16_t operator[](uint16_t *) const;
 inline void operator()(uint16_t *, uint16_t);
 inline void To0(uint16_t *);
 inline void From0To1(uint16_t *);
 inline void From3To1(uint16_t *);
 inline void From3To2(uint16_t *);
 inline void Set_to(uint8_t);
};

_2bitsNDArray_::_2bitsNDArray_(void){
	Dim = 0;
	Size = 0;
}

_2bitsNDArray_::_2bitsNDArray_(uint64_t Dim, uint64_t Size){
 Realloc(Dim, Size);
}

_2bitsNDArray_::~_2bitsNDArray_(){
 Free();
}

inline uint64_t _2bitsNDArray_::size(void) const{
 return this->Size;
}

void _2bitsNDArray_::Realloc(uint64_t Dim,uint64_t size){
 
 this->Size = size;
 this->Dim = Dim;
// this->LineSize = this->Size/(NBits >> bits) +1U;
 this->LineSize = (this->Size >> shifter) + ( this->Size & ElePerItem ? 1U : 0U);
 this-> AllocatedSize = pow( Size, this->Dim-1U)*LineSize;
 Array = (ArrType*)realloc(Array, sizeof(ArrType)*AllocatedSize);
 if( Array == NULL){
	fprintf(stderr, "error reallocating _2bitsNDArray_\n");
	exit(1);
 }
}

void _2bitsNDArray_::Free(void){
 free(Array);
 Array = NULL;
 this->Dim = 0;
 this->Size = 0;
 this->AllocatedSize = 0;
}

inline uint16_t _2bitsNDArray_::operator[](uint16_t *Prt) const{
 uint64_t Index = Prt[0] >> shifter;
 uint64_t u = (uint64_t) 1U;
 for(; u < Dim; u++){
	Index += (uint64_t) LineSize*pow(Size, u-1U)*Prt[u];
 }
// return (Array[Index] >> ((Prt[0] & ElePerItem)*2U)) & 3U;
 u = Prt[0] & ElePerItem;
 return ((Array[Index] & MaskGet[u]) >> u) >> u;
}

inline void _2bitsNDArray_::operator()(uint16_t *Prt, uint16_t value){
 uint64_t Index = Prt[0] >> shifter;
 uint64_t u = (uint64_t) 1U;
 for(; u < Dim; u++){
	Index += (uint64_t) LineSize*pow(Size, u-1U)*Prt[u];
 }
 uint64_t mask = 0;
 switch(value){
	case 0U:
		mask = ~( 3UL <<(Prt[0] & ElePerItem)*2U);
		Array[Index] &= mask;
		break;
	case 1U:
		mask = 1UL <<(Prt[0] & ElePerItem)*2U;
		Array[Index] |= mask;
		mask <<= 1;
		mask = ~mask;
		Array[Index] &= mask;
		break;
	case 2U:
		mask = 2UL <<(Prt[0] & ElePerItem)*2U;
		Array[Index] |= mask;
		mask >>= 1;
		mask = ~mask;
		Array[Index] &= mask;
		break;
	case 3U:
		mask = ( 3UL <<(Prt[0] & ElePerItem)*2U);
		Array[Index] |= mask;
		break;
 }
}

inline void _2bitsNDArray_::To0(uint16_t *Prt){
 uint64_t Index = Prt[0] >> shifter;
 uint64_t u = (uint64_t) 1U;
 for(; u < Dim; u++){
	Index += (uint64_t) LineSize*pow(Size, u-1U)*Prt[u];
 }
 Array[Index] &= MaskTo0[Prt[0] & ElePerItem];
}

inline void _2bitsNDArray_::From0To1(uint16_t *Prt){
 uint64_t Index = Prt[0] >> shifter;
 uint64_t u = (uint64_t) 1U;
 for(; u < Dim; u++){
	Index += (uint64_t) LineSize*pow(Size, u-1U)*Prt[u];
 }
 Array[Index] |= Mask0To1[Prt[0] & ElePerItem];
}

inline void _2bitsNDArray_::From3To1(uint16_t *Prt){
 uint64_t Index = Prt[0] >> shifter;
 uint64_t u = (uint64_t) 1U;
 for(; u < Dim; u++){
	Index += (uint64_t) LineSize*pow(Size, u-1U)*Prt[u];
 }
 Array[Index] &= Mask3To1[Prt[0] & ElePerItem];
}

inline void _2bitsNDArray_::From3To2(uint16_t *Prt){
 uint64_t Index = Prt[0] >> shifter;
 uint64_t u = (uint64_t) 1U;
 for(; u < Dim; u++){
	Index += (uint64_t) LineSize*pow(Size, u-1U)*Prt[u];
 }
 Array[Index] &= Mask3To2[Prt[0] & ElePerItem];
}
inline void _2bitsNDArray_::Set_to(uint8_t value){
 memset(Array, value, AllocatedSize*sizeof(ArrType));
// for(unsigned x = 0;x < AllocatedSize; x++){
//	printf("%lu\n", Array[x]);
// }
}
#endif
