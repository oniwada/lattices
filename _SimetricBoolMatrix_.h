#ifndef _SIMETRICBOOLMATRIX__H
#define _SIMETRICBOOLMATRIX__H

#include<vector>
#include<algorithm>

class _SimetricBoolMatrix_{
	private:
 std::vector<std::vector<uint64_t>> SM;
 const unsigned long long Nbits = sizeof(uint64_t)*8;

	public:
 _SimetricBoolMatrix_(void);
 _SimetricBoolMatrix_(long long);

 inline long long size(void) const;
 void Realloc(long long);
 inline bool Get(long long, long long) const;
 inline void Set(long long, long long, bool);
 inline void Set_to0(void);
};

_SimetricBoolMatrix_::_SimetricBoolMatrix_(void):
	SM(0){
}

_SimetricBoolMatrix_::_SimetricBoolMatrix_(long long size){
 Realloc(size);
}

inline long long _SimetricBoolMatrix_::size(void) const{
 return SM.size();
}

void _SimetricBoolMatrix_::Realloc(long long size){
 try{
	SM.resize(size);
	for(long long index = 0; index < size; index++){
		SM[index].resize(index/Nbits+1);
	}
 }
 catch(...){
	fprintf(stderr, "Error resizing _SimetricBoolMatrix_\n");
	exit(1);
 }
}


inline bool _SimetricBoolMatrix_::Get(long long i, long long j) const{
 if(j > i) return (SM[j][i/Nbits] >> (i%Nbits))&1;
 else{
	return (SM[i][j/Nbits] >> (j%Nbits))&1;
 }
}

inline void _SimetricBoolMatrix_::Set(long long i, long long j, bool value){
 if(j > i){
	if(value){ SM[j][i/Nbits] |= 1UL << (i%Nbits);}
	else{ SM[j][i/Nbits] &= ~(1UL << (i%Nbits));}
 }
 else{
	if(value){ SM[i][j/Nbits] |= 1UL << (j%Nbits);}
	else{ SM[i][j/Nbits] &= ~(1UL << (j%Nbits));}
 }
}

inline void _SimetricBoolMatrix_::Set_to0(void){
 for(std::vector<uint64_t> &arr : SM){
	std::fill(arr.begin(), arr.end(), 0);
 }
}
#endif
