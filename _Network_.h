#ifndef __Network__h
#define __Network__h

#include<vector>
#include<algorithm>
#include<assert.h>

template<typename Type>
class _Network_{
	public:
 long long NTriang = 0;
 std::vector<std::vector<Type>> Vert_list;

 _Network_(Type);
 _Network_(Type, Type);
 inline void Realloc(unsigned long long);
 inline void ChangeFixedDegree(Type);

 void CalculateNTriang(void);
 inline	 long long SiteTriang(Type);
 inline long long Get_NTriang(void) const;
 inline unsigned long Get_NSites(void) const;
 void Set_MaxTriangConfig(Type, Type);

 inline std::vector<Type>& operator[](Type);
};

/* *********************************************

		CONSTRUCTORS
		DESTRUCTORS
		REALLOCATIONS

   ********************************************* */
template<typename Type>
_Network_<Type>::_Network_(Type size):
	Vert_list(size, std::vector<Type>())
{}

template<typename Type>
_Network_<Type>::_Network_(Type NBlocks, Type Degree):
	Vert_list((Degree+1)*NBlocks, std::vector<Type>(Degree)){
 Set_MaxTriangConfig(NBlocks, Degree);
}

template<typename Type>
inline void _Network_<Type>::Realloc(unsigned long long NewNsites){
 if(NewNsites != Get_NSites()){
	try{
		Vert_list.resize(NewNsites, std::vector<Type>());
	}
	catch(...){
		fprintf(stderr, "Error resizing Vert_list\n");
		exit(1);
	}
 }
}

template<typename Type>
inline void _Network_<Type>::ChangeFixedDegree(Type NewDegree){
 for(std::vector<Type> &array : Vert_list){
	try{
		array.resize(NewDegree, 0);
	}
	catch(...){
		fprintf(stderr, "Error resizing Vert_list\n");
		exit(1);
	}
 }
}

/* *********************************************

		INTERNAL CONFIGURATION

   ********************************************* */
template<typename Type>
inline void _Network_<Type>::Set_MaxTriangConfig(Type NBlocks, Type Degree){
 Realloc(NBlocks*(Degree+1));
 ChangeFixedDegree(Degree);
 for(Type block = 0; block < NBlocks*(Degree+1); block+= Degree+1){
	for(Type site_i = 0; site_i < Degree+1; site_i++){
		Type counter = 0;
		for(Type site_j = 0; site_j < Degree+1; site_j++){
			if(site_i == site_j) continue;
			Vert_list[block+site_i][counter++]=block+site_j;
		}
	}
 }
 CalculateNTriang();
}

template <class Type>
void _Network_<Type>::CalculateNTriang(void){
 NTriang = 0;
 for(Type node_i = 0; node_i < Get_NSites(); node_i++){
	for(Type node_j : Vert_list[node_i]){
		if(node_j < node_i){ continue;}
		for(Type node_k : Vert_list[node_i]){
			if(node_k < node_j){ continue;}
			else if( std::binary_search(Vert_list[node_j].begin(), Vert_list[node_j].end(), node_k) ){
				NTriang++;
			}
		}
	}
 }
}

template <typename Type>
inline long long _Network_<Type>::SiteTriang(Type site){
 long long site_triang = 0;
 for(Type node_j : Vert_list[site]){
 for(Type node_k : Vert_list[site]){
	if(node_k < node_j and std::binary_search(Vert_list[node_j].begin(), Vert_list[node_j].end(), node_k) ){
		site_triang++;
	}
 }
 }
 return site_triang;
}
//template <typename Type>
//void _Network_<Type>::Set_FixedDegreeState(Type Degree){
// assert(Degree < Get_NSites());
//}

/* *********************************************

		GET AND SET PRIVATE DATA

   ********************************************* */

template <class Type>
inline long long _Network_<Type>::Get_NTriang(void) const{
 return NTriang;
}

template <typename Type>
inline unsigned long _Network_<Type>::Get_NSites(void) const{
 return Vert_list.size();
}
/* *********************************************

		OVERLOAD

   ********************************************* */

template <class Type>
inline std::vector<Type>& _Network_<Type>::operator[](Type index){
 return Vert_list[index];
}

#endif
