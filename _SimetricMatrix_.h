#ifndef _SIMETRICMATRIX__H
#define _SIMETRICMATRIX__H

#include<vector>

template<typename Type>
class _SimetricMatrix_{
	private:
 std::vector<std::vector<Type>> SM;

	public:
 _SimetricMatrix_(void);
 _SimetricMatrix_(long long);

 inline long long size(void);
 void Realloc(long long);
 inline Type Get(long long, long long);
 inline void Set(long long, long long, Type);
 inline void Set_to0(void);
};

template<typename Type>
_SimetricMatrix_<Type>::_SimetricMatrix_(void):
	SM(0){
}

template<typename Type>
_SimetricMatrix_<Type>::_SimetricMatrix_(long long size){
 Realloc(size);
}


template<typename Type>
inline long long _SimetricMatrix_<Type>::size(void){
 return SM.size();
}


template<typename Type>
void _SimetricMatrix_<Type>::Realloc(long long size){
 try{
	SM.resize(size);
	for(long long index = 0; index < size; index++){
		SM[index].resize(index+1);
	}
 }
 catch(...){
	fprintf(stderr, "Error resizing _SimetricMatrix_\n");
	exit(1);
 }
}

template<typename Type>
inline Type _SimetricMatrix_<Type>::Get(long long i, long long j){
 if(j > i) return SM[j][i];
 else{
	return SM[i][j];
 }
}

template<typename Type>
inline void _SimetricMatrix_<Type>::Set(long long i, long long j, Type value){
 if(j > i) SM[j][i] = value;
 else{
	SM[i][j] = value;
 }
}

template<typename Type>
inline void _SimetricMatrix_<Type>::Set_to0(void){
 for(std::vector<Type> &arr : SM){
	std::fill(arr.begin(), arr.end(), 0);
 }
}
#endif
