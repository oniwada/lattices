#ifndef _bitNDArray__H
#define _bitNDArray__H

#include<vector>
#include<algorithm>
#include<math.h>
#include<assert.h>


class _bitNDArray_{
	private:
// typedef uint64_t ArrType;
 const uint8_t MaskTo0[4]  = {0xFC, 0xF3, 0xCF, 0x3F};
 const uint8_t Mask0To1[4] = {0x01, 0x04, 0x10, 0x40};
 const uint8_t Mask3To1[4] = {0xFD, 0xF7, 0xDF, 0x7F};
 const uint8_t Mask3To2[4] = {0xFE, 0xFB, 0xEF, 0xBF};
 const uint8_t MaskGet[4] = {0x03, 0x0C, 0x30, 0xC0};
 typedef uint8_t ArrType;
 uint64_t Dim = 0,
          Size = 0,
          LineSize = 0,
          AllocatedSize = 0;
 ArrType *Array = NULL;
 const uint64_t ElePerItem = 7;
 const uint64_t shifter = 3;

	public:
 _bitNDArray_(void);
 _bitNDArray_(uint64_t, uint64_t);
 ~_bitNDArray_();

 inline uint64_t size(void) const;
 void Realloc(uint64_t, uint64_t);
 void Free(void);

 inline uint16_t operator[](uint16_t *) const;
 inline void To0(uint16_t *);
 inline void To1(uint16_t *);

 inline uint32_t operator[](uint32_t *) const;
 inline void To0(uint32_t *);
 inline void To1(uint32_t *);

 inline void Set_to(uint8_t);
};

_bitNDArray_::_bitNDArray_(void){
	Dim = 0;
	Size = 0;
}

_bitNDArray_::_bitNDArray_(uint64_t Dim, uint64_t Size){
 Realloc(Dim, Size);
}

_bitNDArray_::~_bitNDArray_(){
 Free();
}

inline uint64_t _bitNDArray_::size(void) const{
 return this->Size;
}

void _bitNDArray_::Realloc(uint64_t Dim,uint64_t size){
 
 this->Size = size;
 this->Dim = Dim;
// this->LineSize = this->Size/(NBits >> bits) +1U;
 this->LineSize = (this->Size >> shifter) + ( this->Size & ElePerItem ? 1U : 0U);
 this-> AllocatedSize = pow( Size, this->Dim-1U)*LineSize;
 Array = (ArrType*)realloc(Array, sizeof(ArrType)*AllocatedSize);
 if( Array == NULL){
	fprintf(stderr, "error reallocating _bitNDArray_\n");
	exit(1);
 }
}

void _bitNDArray_::Free(void){
 free(Array);
 Array = NULL;
 this->Dim = 0;
 this->Size = 0;
 this->AllocatedSize = 0;
}

inline uint16_t _bitNDArray_::operator[](uint16_t *Prt) const{
 uint64_t Index = Prt[0] >> shifter;
 uint64_t u = (uint64_t) 1U;
 for(; u < Dim; u++){
	Index += (uint64_t) LineSize*pow(Size, u-1U)*Prt[u];
 }
 u = Prt[0] & ElePerItem;
 return (Array[Index] >> u) & 1U;
}


inline void _bitNDArray_::To0(uint16_t *Prt){
 uint64_t Index = Prt[0] >> shifter;
 uint64_t u = (uint64_t) 1U;
 for(; u < Dim; u++){
	Index += (uint64_t) LineSize*pow(Size, u-1U)*Prt[u];
 }
 Array[Index] &= ~(1U << (Prt[0] & ElePerItem));
}

inline void _bitNDArray_::To1(uint16_t *Prt){
 uint64_t Index = Prt[0] >> shifter;
 uint64_t u = (uint64_t) 1U;
 for(; u < Dim; u++){
	Index += (uint64_t) LineSize*pow(Size, u-1U)*Prt[u];
 }
 Array[Index] |= 1U << (Prt[0] & ElePerItem);
}

inline uint32_t _bitNDArray_::operator[](uint32_t *Prt) const{
 uint64_t Index = Prt[0] >> shifter;
 uint64_t u = (uint64_t) 1U;
 for(; u < Dim; u++){
	Index += (uint64_t) LineSize*pow(Size, u-1U)*Prt[u];
 }
 u = Prt[0] & ElePerItem;
 return (Array[Index] >> u) & 1U;
}


inline void _bitNDArray_::To0(uint32_t *Prt){
 uint64_t Index = Prt[0] >> shifter;
 uint64_t u = (uint64_t) 1U;
 for(; u < Dim; u++){
	Index += (uint64_t) LineSize*pow(Size, u-1U)*Prt[u];
 }
 Array[Index] &= ~(1U << (Prt[0] & ElePerItem));
}

inline void _bitNDArray_::To1(uint32_t *Prt){
 uint64_t Index = Prt[0] >> shifter;
 uint64_t u = (uint64_t) 1U;
 for(; u < Dim; u++){
	Index += (uint64_t) LineSize*pow(Size, u-1U)*Prt[u];
 }
 Array[Index] |= 1U << (Prt[0] & ElePerItem);
}

inline void _bitNDArray_::Set_to(uint8_t value){
 memset(Array, value, AllocatedSize*sizeof(ArrType));
}
#endif
