#ifndef __3DSquareLattice__
#define __3DSquarelattice__

#include<stdlib.h>
#include<assert.h>
#include<string.h>

template <typename Type>
class _3DSquareLattice_{
	public:
 typedef uint_fast32_t _FastIntType_;

 _3DSquareLattice_(_FastIntType_);
 _3DSquareLattice_(void);
 ~_3DSquareLattice_();
 
 void Init(_FastIntType_);
 void Free(void);
 void Realloc(_FastIntType_);
 void Set_to_0(void);
 inline Type** operator[](_FastIntType_);
 inline _FastIntType_ get_size(void);

	private:
 Type ***Array = NULL;
 _FastIntType_ size= 0U;
};

template <typename Type>
_3DSquareLattice_<Type>::_3DSquareLattice_(_FastIntType_ InputSize){
 Init(InputSize);
}

template <typename Type>
_3DSquareLattice_<Type>::_3DSquareLattice_(void){
 Init(1);
}

template <typename Type>
_3DSquareLattice_<Type>::~_3DSquareLattice_(){
 if(this->size)
	Free();
}

template <typename Type>
void _3DSquareLattice_<Type>::Init(_FastIntType_ InputSize){
 assert(InputSize > 0U && size == 0U);
 this->size = InputSize;
 Array = (Type***)malloc(sizeof(Type**)*size);
 if( Array == NULL){
	fprintf(stderr,"error allocating Array in _3DSquareLattice_\n\n");
	exit(1);
 }
 for(_FastIntType_ y = 0U; y < size; y++){
	Array[y] = (Type**)malloc(sizeof(Type*)*size);
	if(Array[y] == NULL){
		fprintf(stderr,"error allocating Array in _3DSquareLattice_\n\n");
		exit(1);
	}
	for(_FastIntType_ z = 0U; z < size; z++){
		Array[y][z] = (Type*)calloc(size,sizeof(Type));
		if(Array[y][z] == NULL){
			fprintf(stderr,"error allocating Array in _3DSquareLattice_\n\n");
			exit(1);	
		}
	}
 }
}

template <typename Type>
void _3DSquareLattice_<Type>::Realloc(_FastIntType_ InputSize){
 if(InputSize != this->size){
	Free();
	Init(InputSize);
 }
}

template <typename Type>
void _3DSquareLattice_<Type>::Free(void){
 if( Array != NULL){
	for(_FastIntType_ y = 0U; y < this->size; y++){
		for(_FastIntType_ z = 0U; z < this->size; z++){
			free(Array[y][z]);
			Array[y][z] = NULL;
		}
		free(Array[y]);
		Array[y] = NULL;
	}
 }
 this->size = 0U;
 free(Array);
 Array = NULL;
}

template <typename Type>
inline void _3DSquareLattice_<Type>::Set_to_0(void){
 _FastIntType_ x, y, z;
 for(x = 0; x < this->size; x++)
	for(y = 0; y < this->size; y++)
		memset(Array[x][y], 0,sizeof(Type)*this->size);
}

template <typename Type>
inline Type** _3DSquareLattice_<Type>::operator[](_FastIntType_ index){
 return Array[index];
}

template <typename Type>
inline uint_fast32_t _3DSquareLattice_<Type>::get_size(void){
 return this->size;
}

#endif
